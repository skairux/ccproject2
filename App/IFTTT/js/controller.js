var app = angular.module('fetchModule', ['toaster']);

function refreshPostAnimation($website){

if ($website == "twitter")
	$markup = ".twitterContents p .twitterText";
else if ($website == "facebook")
	$markup = ".date, .message, .like, .image";
else 
	$markup = "";

	var langue = localStorage['langue'] != undefined ? localStorage['langue'] : 'FR';
	$markup.fadeToggle("fast").delay(1000, function(){
	$markup.fadeToggle("fast");
});

}

app.controller('twitterCtrl', function ($scope, $http, toaster, $interval) {
		old_tweet    = [];
		actual_tweet = [];
		$http
		.get('script/twitter.php')
		.success(function(data){	
			$scope.tweetList = actual_tweet = angular.fromJson(data);
			old_tweet        = actual_tweet[0].tweet;
		});
		$interval(function() {
			$http
			.get('script/twitter.php')
			.success(function(data){	
				actual_tweet = angular.fromJson(data);
				if (actual_tweet[0].tweet != old_tweet) {
					refreshPostAnimation("twitter");
				}
			});

			if (actual_tweet[0].tweet != old_tweet) {
				notif = actual_tweet[0].tweet.substr(0, 100);
				console.log("123456789".substr(0,2));
				toaster.pop('note', "Nouveau tweet de @"+actual_tweet[0].screenname, notif+"...");
				$scope.tweetList = actual_tweet;
			}

			old_tweet = actual_tweet[0].tweet;
		},10000);

});

app.controller('instaCtrl', function ($scope, $http, toaster, $interval) {
	old_pic    = [];
	actual_pic = [];
	$http	
	.get('script/instagram.php')
	.success(function(data2){    
		$scope.instaFeed = actual_pic = angular.fromJson(data2);
		old_pic          = actual_pic[0].picture;
	});
	$interval(function() {
		$http	
		.get('script/instagram.php')
		.success(function(data2){    
			actual_pic   = angular.fromJson(data2);
			if (actual_pic[0].picture != old_pic) {
				refreshPostAnimation("instagram");
			 }
		});

		if (actual_pic[0].picture != old_pic) {
			notif = actual_pic[0].comment.substr(0, 100);
			toaster.pop('note', "Nouvelle photo de "+ actual_pic[0].pseudo, notif2+"...");
			$scope.instaFeed = actual_pic;
		}
		old_pic = actual_pic[0].picture;
	}, 10000);
});

app.controller('fcbkCtrl', function ($scope, $http, toaster, $interval) {
	old_statut 	  = [];
	actual_statut = [];
	$http
	.get('script/facebook.php')
	.success(function(data3){
		$scope.statutFeed = angular.fromJson(data3);
		// old_statut 		  = actual_statut[0].message;
	});
	$interval(function(){
		$http
		.get('script/facebook.php')
		.success(function(data3){
			actual_statut = angular.fromJson(data3);
			if (actual_statut[0].message != old_statut) {
				console.log(actual_statut[0]);
				refreshPostAnimation("facebook");
			 }
		});

		if (actual_statut[0].message != old_statut) {
			notif3 = actual_statut[0].message.substr(0, 100);
			toaster.pop('note', "Nouveau statut de "+ actual_statut[0].name, notif3+"...");
			$scope.statutFeed = actual_statut;
		};
		old_statut = actual_statut[0].message;
	}, 10000);
});